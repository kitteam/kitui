var pkg = require('./package.json');

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify-css');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var header = require('gulp-header');
var del = require('del');
var gulpif = require('gulp-if');
var minimist = require('minimist');
var zip = require('gulp-zip');
var less = require('gulp-less');
var notify = require('gulp-notify');
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var tsify = require("tsify");

var paths = {
  pages: ['./src/*.html', './src/*/*.html', './src/*/js/*.js']
};

// 任务管理
var task = {
  // css 压缩
  mincss: function() {
    return gulp.src(['./src/css/*.css'])
      .pipe(minify())
      .pipe(header.apply(null, ['/** <%= pkg.name %>-v<%= pkg.version %> <%= pkg.license %> License By <%= pkg.homepage %> e-mail:<%= pkg.email %> */\n', { pkg: pkg }]))
      .pipe(gulp.dest('./dist/css'))
      .pipe(notify({ message: 'mincss task ok' }));
  },
  // js 压缩
  minjs: function() {
    return gulp.src(['./src/js/*.js', './src/js/**/*.js'])
      // .pipe(gulpif('!app.js', uglify()))
      // .pipe(babel())
      .pipe(uglify())
      .pipe(header.apply(null, ['/** <%= pkg.name %>-v<%= pkg.version %> <%= pkg.license %> License By <%= pkg.homepage %> e-mail:<%= pkg.email %> */\n <%= js %>', { pkg: pkg, js: ';' }]))
      .pipe(gulp.dest('./dist/js'))
      .pipe(notify({ message: 'minjs task ok' }));
  },
  file: function() {
    return gulp.src(['./src/images/**/*.{png,jpg,gif,html,mp3,json}'])
      .pipe(rename({}))
      .pipe(gulp.dest('./dist/images'))
      .pipe(notify({ message: 'file task ok' }));
  },
  // 将less转成css
  less2css: function() {
    return gulp.src('./src/css/*.less')
      .pipe(less())
      .pipe(gulp.dest('./src/css'))
      .pipe(notify({ message: 'less2css task ok' }));
  },
  // 将ts转成js
  ts2js: function() {
    return tsProject.src()
      .pipe(tsProject())
      .js.pipe(gulp.dest("./src/js"))
      .pipe(notify({ message: 'ts2js task ok' }));
  },
  // 将html文件复制到dist文件夹
  copyhtml: function() {
    return gulp.src(paths.pages)
      .pipe(gulp.dest("dist"));
  },
  // tshtmlhandler: function() {
  //     return browserify({
  //             basedir: '.',
  //             debug: true,
  //             entries: ['src/ts/index.ts'],
  //             cache: {},
  //             packageCache: {}
  //         })
  //         .plugin(tsify)
  //         .bundle()
  //         .pipe(source('bundle.js'))
  //         .pipe(gulp.dest("./src/js"))
  //         .pipe(notify({ message: 'tshtmlhandler task ok' }));
  // }
};
gulp.task('less2css', task.less2css);
gulp.task('minjs', task.minjs);
gulp.task('mincss', task.mincss);
gulp.task('file', task.file);
gulp.task('ts2js', ["copyhtml"], task.ts2js);
// gulp.task('tshtmlhandler', ["ts2js", "copyhtml"], task.tshtmlhandler);
gulp.task('copyhtml', task.copyhtml);
gulp.task('default', function() {
  //
});
gulp.task('all', ['clear'], function() {
  for (var key in task) {
    task[key]();
  }
});
gulp.task('clear', function(cb) {
  return del(['./dist/*'], cb);
});