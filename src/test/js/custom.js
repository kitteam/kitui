kitui.define(['basic'], function(basic) {

    var basic = new basic.default();
    //basic.hello();

    return function() {

        this.helloWorld = function() {

            basic.hello();

            console.log("helloWorld");

            return "return hello world.";
        }
    };
});