class Menu {
    render(): void {
        let menus: NodeListOf<Element> = document.querySelectorAll(".kit-menu");
        for (let menu in menus) {
            // .hasOwnProperty函数用于指示一个对象自身(不包括原型链)是否具有指定名称的属性
            if (menus.hasOwnProperty(menu)) {
                let element: Element = menus[menu];
                // 获取当前menu下的所有item
                let elems: NodeListOf<Element> = element.querySelectorAll(".kit-item");
                for (let item in elems) {
                    if (elems.hasOwnProperty(item)) {
                        let e: Element = elems[item];
                        e.querySelector(".kit-title").addEventListener("click", function (): void {
                            if (e.querySelector(".kit-menu-sub") !== null) {
                                // 阻止事件冒泡
                                arguments[0].stopPropagation();
                                // 当前菜单是否为打开状态
                                let hasOpen: boolean = e.className.search("kit-open") !== -1;
                                // 切换菜单的展开和隐藏
                                e.className = hasOpen ? "kit-item" : "kit-item kit-open";
                            } else {
                                // 清除所有选中的选项样式
                                for (let key in elems) {
                                    if (elems.hasOwnProperty(key)) {
                                        let e1: HTMLElement = elems[key] as HTMLElement;
                                        let hasOpen: boolean = e1.className.search("kit-open") !== -1;
                                        // 全设置为未选中状态
                                        e1.className = !hasOpen ? "kit-item" : "kit-item kit-open";
                                    }
                                }
                                // 切换选中状态
                                e.classList.contains("kit-active") ? e.classList.remove("kit-active") : e.classList.add("kit-active");
                            }
                        });
                    }
                }
            }
        }
    }
}
export default Menu;