class Dropdown {
    render(): void {
        let dropdowns: NodeListOf<HTMLElement> = document.querySelectorAll("[data-toggle=kit-dropdown]") as NodeListOf<HTMLElement>;
        for (let key in dropdowns) {
            if (dropdowns.hasOwnProperty(key)) {
                let dropdown: HTMLElement = dropdowns[key];
                dropdown.addEventListener("click", function (e: any): void {
                    // 阻止事件冒泡
                    e.stopPropagation();
                    // 获取子下拉列表节点
                    let child: HTMLElement = dropdown.querySelector(".kit-dropdown") as HTMLElement;
                    if (child !== null) {
                        let classList: any = child.classList;
                        let showClass: string = "kit-dropdown-show";
                        // 切换状态
                        classList.contains(showClass) ? classList.remove(showClass) : classList.add(showClass);
                        let handler: any = function (): void {
                            child.classList.remove("kit-dropdown-show");
                            // 移除当前事件
                            document.removeEventListener("click", handler);
                        };
                        document.addEventListener("click", handler);
                    }
                });
            }
        }
    }
}

export default Dropdown;