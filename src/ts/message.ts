interface IConfig {
    text?: string;
    elem?: string;
}
class Message {
    /**
     * 打开一个加载层
     * @param options 参数
     */
    spin(options?: IConfig): number {
        let times: number = new Date().getTime();
        let text: string = "加载中.";
        let _elem: HTMLElement = document.querySelector("body");
        if (options !== undefined) {
            if (options.text !== undefined && options.text.trim().length > 0) {
                text = options.text;
            }
            if (options.elem !== undefined
                && options.elem.trim().length > 0
                && document.querySelector(options.elem) !== undefined) {
                _elem = document.querySelector(options.elem) as HTMLElement;
            }
        }
        let _msg: HTMLElement = document.createElement("div");
        _msg.className = "kit-message kit-anim kit-anim-fadein";
        _msg.setAttribute("data-times", times.toString());
        let _shade: HTMLElement = document.createElement("div");
        _shade.classList.add("kit-shade");
        let _spin: HTMLElement = document.createElement("div");
        _spin.classList.add("kit-spin");
        let _spin_icon: HTMLElement = document.createElement("span");
        _spin_icon.classList.add("kit-spin-icon");
        let _spin_i: HTMLElement = document.createElement("i");
        _spin_i.className = "fa fa-spinner";
        _spin_i.setAttribute("aria-hidden", "true");
        let _spin_text: HTMLElement = document.createElement("p");
        _spin_text.classList.add("kit-spin-text");
        _spin_text.innerText = text;
        _spin_icon.appendChild(_spin_i);
        _spin.appendChild(_spin_icon);
        _spin.appendChild(_spin_text);
        _msg.appendChild(_shade);
        _msg.appendChild(_spin);
        _elem.appendChild(_msg);
        return times;
    }
    /**
     * 销毁指定modal
     * @param _elem dom
     */
    destroy(_elem: HTMLElement): void {
        _elem.classList.remove("kit-anim-fadein");
        _elem.classList.add("kit-anim-fadeout");
        // 设置一秒后移除dom,目的是为了让动画有一个播放的过程.
        setTimeout(function (): void {
            _elem.remove();
        }, 1000);
    }
    /**
     * 移除指定层
     * @param times id
     */
    close(times: number): void {
        let that:Message = this;
        let elements: NodeListOf<HTMLElement> = document.querySelectorAll("div[data-times=\"" + times + "\"]") as NodeListOf<HTMLElement>;
        for (let key in elements) {
            if (elements.hasOwnProperty(key)) {
                that.destroy(elements[key]);
            }
        }
    }
    /**
     * 移除所有层
     */
    closeAll(): void {
        let that:Message = this;
        let elements: NodeListOf<HTMLElement> = document.querySelectorAll("div[data-times]") as NodeListOf<HTMLElement>;
        for (let key in elements) {
            if (elements.hasOwnProperty(key)) {
                that.destroy(elements[key]);
            }
        }
    }
}
export default Message;