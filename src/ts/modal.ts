import Utils from "./utils";

/**
 * 参数定义
 */
interface IOption {
  title: any;
  content: string;
  shade: boolean;
  // 事件
  // dom销毁之后发生
  onDestroyed: any;
  // 层关闭后发生
  onClosed: any;
  // 层渲染成功后发生
  onSuccessed: any;
  // 是否启用点击遮盖层关闭
  shadeClose: boolean;
  // 动画类型
  anim: AnimateType;
}
/**
 * 载入动画类型
 */
enum AnimateType {
  Fade,
  Bounce,
  FilpY,
  FilpX,
  Silde,
  SildeLeft,
}
/**
 * 载入动画信息
 */
interface IAnimateInfo {
  /**
   * 载入动画的clsssName
   */
  in: string;
  /**
   * 退出动画的className
   */
  out: string;
}
/**
 * 模型框组件
 */
class Modal {
  /**
   *  预设的载入动画类型列表
   */
  AnimateList: Array<[string, string]>;
  /**
   *  默认参数
   */
  defaults: IOption = {
    title: "提示",
    content: "内容一",
    shade: true,
    onDestroyed: undefined,
    onClosed: undefined,
    onSuccessed: undefined,
    anim: AnimateType.Fade,
    shadeClose: true
  };
  /**
   * 构造函数
   */
  constructor() {
    // 初始化
    this.AnimateList = [
      ["fadeIn", "fadeOut"],
      ["bounceIn", "bounceOut"],
      ["flipInX", "flipOutX"],
      ["flipInY", "flipOutY"],
      ["slideInDown", "slideOutUp"],
      ["slideInLeft", "slideOutRight"],
      ["kit-anim-fadein", "kit-anim-fadeout"],
      ["kit-anim-smoothin", "kit-anim-smoothout"],
    ];
  }
  /**
   * 警告框
   * @param options 参数
   */
  alert(options: any): number {
    return 1;
  }
  /**
   * modal 弹出层
   * @param options 参数
   */
  dialog(options: IOption): number {
    let _that: Modal = this;
    // options = new Utils().merge(_that.defaults, options);
    console.log(options);
    // 获取modal的唯一id
    let times: number = new Date().getTime();
    // 载入动画的默认设置
    let anim: IAnimateInfo = {
      in: _that.AnimateList[0][0],
      out: _that.AnimateList[0][1]
    };
    let animId: number = 0;
    if (options !== null && options.anim !== undefined && typeof options.anim === "number") {
      try {
        // 如果设置的值合法则配置合法的值，否则使用默认值
        anim = {
          in: _that.AnimateList[options.anim][0],
          out: _that.AnimateList[options.anim][1]
        };
        animId = options.anim;
        // tslint:disable-next-line:no-empty
      } catch { }
    }
    // 创建modal最外层
    let _modal: HTMLElement = document.createElement("div");
    _modal.className = `kit-modal`;
    _modal.setAttribute("data-times", times.toString());
    _modal.setAttribute("data-anim", animId.toString());
    console.log((typeof options.title === "boolean" && options.title));
    // 元素隐藏
    let shade_c: string = options.shade === undefined || options.shade ? "" : "kit-hide";
    let title_c: string = options.title !== undefined &&
      (typeof options.title === "boolean" && !options.title) ? "kit-hide" : "";
    let foot_c: string = "";
    // 内容
    let title: string = options.title !== undefined ? options.title : "提示";
    // 配置modal内容
    let _arr: Array<string> = [
      `<div class="kit-modal-shade kit-anim kit-anim-fadein ${shade_c}"></div>`,
      `<div class="kit-modal-dialog kit-anim ${anim.in}">`,
      `<div class=\"kit-title\  ${title_c}">`,
      `<span>${title}</span>`,
      "<span class=\"kit-close\">",
      "<i class=\"fa fa-close\" aria-hidden=\"true\"></i>",
      "</span>",
      "</div>",
      "<div class=\"kit-body\">",
      "This is Content...",
      "</div>",
      "<div class=\"kit-footer\">",
      "<button type=\"button\" class=\"kit-btn kit-btn-info kit-btn-sm\">确定</button>",
      "<button type=\"button\" class=\"kit-btn kit-btn-default kit-btn-sm\">取消</button>",
      "</div>"
    ];
    _modal.innerHTML = _arr.join("");
    // 追加到body末尾
    document.querySelector("body").appendChild(_modal);
    if (options !== undefined && options.onSuccessed !== undefined
      && typeof options.onSuccessed === "function") {
      // 渲染成功后触发
      options.onSuccessed(_modal, times);
    }
    // 点击遮盖层关闭
    if (options.shadeClose) {
      _modal.addEventListener("click", function (e: any): void {
        e.stopPropagation();
        if (e.target.className.search("kit-modal") !== -1) {
          _that.destroy(_modal);
          triggerDestroyed();
        }
      });
    }
    // 绑定右上角关闭按钮事件
    _modal.querySelector(".kit-close").addEventListener("click", function (e: any): void {
      e.stopPropagation();
      _that.destroy(_modal);
      triggerDestroyed();
    });
    // 触发销毁后事件
    function triggerDestroyed(): void {
      if (options !== undefined && options.onDestroyed !== undefined
        && typeof options.onDestroyed === "function") {
        options.onDestroyed();
      }
    }
    return times;
  }
  /**
   * 销毁指定modal
   * @param _elem dom
   */
  destroy(_elem: HTMLElement): void {
    let that: Modal = this;
    // 获取设置的加载动画值
    let animId: any = _elem.getAttribute("data-anim");
    // 获取对应的class
    let anim: IAnimateInfo = {
      in: that.AnimateList[animId][0],
      out: that.AnimateList[animId][1]
    };
    let _dialog: HTMLElement = _elem.querySelector(".kit-modal-dialog") as HTMLElement;
    // 移除进入动画
    _dialog.classList.remove(anim.in);
    // 添加退出动画
    _dialog.classList.add(anim.out);
    let _shade: HTMLElement = _elem.querySelector(".kit-modal-shade") as HTMLElement;
    _shade.classList.remove("kit-anim-fadein");
    _shade.classList.add("kit-anim-fadeout");
    // 设置一秒后移除dom,目的是为了让动画有一个播放的过程.
    setTimeout(function (): void {
      _elem.remove();
    }, 1000);
  }
  /**
   * 移除指定层
   * @param times id
   */
  close(times: number): void {
    let _that: Modal = this;
    let elements: NodeListOf<HTMLElement> = document.querySelectorAll("div[data-times=\"" + times + "\"]") as NodeListOf<HTMLElement>;
    for (let key in elements) {
      if (elements.hasOwnProperty(key)) {
        let element: HTMLElement = elements[key];
        _that.destroy(element);
      }
    }
  }
  /**
   * 移除所有层
   */
  closeAll(): void {
    let _that: any = this;
    let elements: NodeListOf<HTMLElement> = document.querySelectorAll("div[data-times]") as NodeListOf<HTMLElement>;
    for (let key in elements) {
      if (elements.hasOwnProperty(key)) {
        let element: HTMLElement = elements[key];
        _that.destroy(element);
      }
    }
  }
}

export default Modal;