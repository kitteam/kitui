import Dropdown from "./dropdown";
interface IGlobal {
}

class Basic {
    global: IGlobal;
    constructor() {
        this.global = {
        };
    }
    hello(): void {
        console.log("hello");
    }
    /**
     *  渲染
     */
    render(): void {
        this.useEffect()
            .useRightMenu();
    }
    /**
     *  处理顶部右侧菜单的功能操作
     */
    useRightMenu(): any {
        let that: any = this;
        let _admin:HTMLElement = document.querySelector(".kit-layout-admin") as HTMLElement;
        let _topbar:HTMLElement =_admin.querySelector(".kit-topbar") as HTMLElement;

        // user info
        new Dropdown().render();

        // setting slider
        let _slider: HTMLElement = _topbar.querySelector("[data-toggle=kit-slider]") as HTMLElement;
        _slider.addEventListener("click", function (e: any): void {
            e.stopPropagation();
            let _right: HTMLElement = _admin.querySelector(".kit-bar-right") as HTMLElement;
            // 切换右侧面板状态
            _right.classList.contains("kit-show")?_right.classList.remove("kit-show"):_right.classList.add("kit-show");
            let handler: any = function (e: any): void {
                // 除开点击自己之外的点击都关闭
                if (e.target.className.search("kit-bar-right") === -1) {
                    // 隐藏右侧面板
                    _right.classList.remove("kit-show");
                    // 移除当前事件
                    document.removeEventListener("click", handler);
                }
            };
            document.addEventListener("click", handler);
        });
        return that;
    }
    /**
     *  处理顶部切换按钮功能
     */
    useEffect(): any {
        let that: any = this; // 获取当前类的上下文对象
        let admin: string = ".kit-layout-admin";
        let topbar: string = ".kit-topbar";
        let effect: string = ".kit-effect";
        let silde: string = ".kit-slide";
        let menu: string = ".kit-menu";
        let body: string = ".kit-body";
        let _admin: HTMLElement = document.querySelector(admin) as HTMLElement;
        let _topbar: HTMLElement = _admin.querySelector(topbar) as HTMLElement;
        let _effect: HTMLElement = _topbar.querySelector(effect) as HTMLElement;
        let _silde: HTMLElement = _admin.querySelector(silde) as HTMLElement;
        let _menu: HTMLElement = _silde.querySelector(menu) as HTMLElement;
        let _body: HTMLElement = _admin.querySelector(body) as HTMLElement;
        _body.style.height = window.innerHeight - 70 + "px";
        _effect.addEventListener("click", function (): void {
            if (_effect.className.search("kit-hide") === -1) {
                _silde.style.marginLeft = "-200px";
                _body.style.paddingLeft = "10px";
                _effect.className = "kit-menu-item kit-effect kit-hide";
            } else {
                _silde.style.marginLeft = "0px";
                _body.style.paddingLeft = "210px";
                _effect.className = "kit-menu-item kit-effect";
            }
        });
        return that;
    }
}

export default Basic;