define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Menu = /** @class */ (function () {
        function Menu() {
        }
        Menu.prototype.render = function () {
            var menus = document.querySelectorAll(".kit-menu");
            var _loop_1 = function (menu) {
                // .hasOwnProperty函数用于指示一个对象自身(不包括原型链)是否具有指定名称的属性
                if (menus.hasOwnProperty(menu)) {
                    var element = menus[menu];
                    // 获取当前menu下的所有item
                    var elems_1 = element.querySelectorAll(".kit-item");
                    var _loop_2 = function (item) {
                        if (elems_1.hasOwnProperty(item)) {
                            var e_1 = elems_1[item];
                            e_1.querySelector(".kit-title").addEventListener("click", function () {
                                if (e_1.querySelector(".kit-menu-sub") !== null) {
                                    // 阻止事件冒泡
                                    arguments[0].stopPropagation();
                                    // 当前菜单是否为打开状态
                                    var hasOpen = e_1.className.search("kit-open") !== -1;
                                    // 切换菜单的展开和隐藏
                                    e_1.className = hasOpen ? "kit-item" : "kit-item kit-open";
                                }
                                else {
                                    // 清除所有选中的选项样式
                                    for (var key in elems_1) {
                                        if (elems_1.hasOwnProperty(key)) {
                                            var e1 = elems_1[key];
                                            var hasOpen = e1.className.search("kit-open") !== -1;
                                            // 全设置为未选中状态
                                            e1.className = !hasOpen ? "kit-item" : "kit-item kit-open";
                                        }
                                    }
                                    // 切换选中状态
                                    e_1.classList.contains("kit-active") ? e_1.classList.remove("kit-active") : e_1.classList.add("kit-active");
                                }
                            });
                        }
                    };
                    for (var item in elems_1) {
                        _loop_2(item);
                    }
                }
            };
            for (var menu in menus) {
                _loop_1(menu);
            }
        };
        return Menu;
    }());
    exports.default = Menu;
});
