define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Dropdown = /** @class */ (function () {
        function Dropdown() {
        }
        Dropdown.prototype.render = function () {
            var dropdowns = document.querySelectorAll("[data-toggle=kit-dropdown]");
            var _loop_1 = function (key) {
                if (dropdowns.hasOwnProperty(key)) {
                    var dropdown_1 = dropdowns[key];
                    dropdown_1.addEventListener("click", function (e) {
                        // 阻止事件冒泡
                        e.stopPropagation();
                        // 获取子下拉列表节点
                        var child = dropdown_1.querySelector(".kit-dropdown");
                        if (child !== null) {
                            var classList = child.classList;
                            var showClass = "kit-dropdown-show";
                            // 切换状态
                            classList.contains(showClass) ? classList.remove(showClass) : classList.add(showClass);
                            var handler_1 = function () {
                                child.classList.remove("kit-dropdown-show");
                                // 移除当前事件
                                document.removeEventListener("click", handler_1);
                            };
                            document.addEventListener("click", handler_1);
                        }
                    });
                }
            };
            for (var key in dropdowns) {
                _loop_1(key);
            }
        };
        return Dropdown;
    }());
    exports.default = Dropdown;
});
