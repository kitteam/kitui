define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * Utils帮助类
     */
    var Utils = /** @class */ (function () {
        function Utils() {
        }
        /**
         * 合并对象
         * @param target 目标对象
         * @param sources 源对象
         */
        Utils.prototype.merge = function (target) {
            var sources = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                sources[_i - 1] = arguments[_i];
            }
            // return Object.assign(target, sources);
        };
        return Utils;
    }());
    exports.default = Utils;
});
