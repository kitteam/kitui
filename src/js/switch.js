define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Switch = /** @class */ (function () {
        function Switch() {
        }
        Switch.prototype.hello = function (name) {
            alert("hello," + name);
        };
        Switch.prototype.render = function (options) {
            // 获取所有的switch按钮
            var switchs = document.querySelectorAll("input[type=checkbox].kit-switch");
            var _loop_1 = function (i) {
                // 获取当前的switch
                var cbx = switchs[i];
                // 获取当前的选中状态
                var checked = cbx.checked;
                // 创建div标签
                var div = document.createElement("div");
                // 给div标签添加className
                div.className = "kit-switched";
                // 创建span标签
                var span = document.createElement("span");
                // 创建em标签
                var em = document.createElement("em");
                // 设置相关参数
                if (checked) {
                    span.className = "kit-switched-checked";
                    em.innerText = "ON";
                }
                else {
                    em.innerText = "OFF";
                }
                // 获取当前switch标签的父级节点
                var p = cbx.parentNode;
                span.appendChild(em);
                div.appendChild(cbx);
                div.appendChild(span);
                p.appendChild(div);
                // 监听switch点击事件
                div.addEventListener("click", function () {
                    var first = this.childNodes[0];
                    var span = this.childNodes[1];
                    var em = span.childNodes[0];
                    if (first.checked) {
                        span.className = "";
                        em.innerText = "OFF";
                        first.removeAttribute("checked");
                    }
                    else {
                        span.className = "kit-switched-checked";
                        em.innerText = "ON";
                        first.setAttribute("checked", "checked");
                    }
                    if (options !== undefined && typeof options.onSwitched === "function") {
                        // 触发回调函数
                        options.onSwitched({
                            elem: first,
                            othis: span,
                            parent: div
                        });
                    }
                });
            };
            // 循环处理
            for (var i = 0; i < switchs.length; i++) {
                _loop_1(i);
            }
        };
        return Switch;
    }());
    exports.default = Switch;
});
