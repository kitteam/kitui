define(["require", "exports", "./dropdown"], function (require, exports, dropdown_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Basic = /** @class */ (function () {
        function Basic() {
            this.global = {};
        }
        Basic.prototype.hello = function () {
            console.log("hello");
        };
        /**
         *  渲染
         */
        Basic.prototype.render = function () {
            this.useEffect()
                .useRightMenu();
        };
        /**
         *  处理顶部右侧菜单的功能操作
         */
        Basic.prototype.useRightMenu = function () {
            var that = this;
            var _admin = document.querySelector(".kit-layout-admin");
            var _topbar = _admin.querySelector(".kit-topbar");
            // user info
            new dropdown_1.default().render();
            // setting slider
            var _slider = _topbar.querySelector("[data-toggle=kit-slider]");
            _slider.addEventListener("click", function (e) {
                e.stopPropagation();
                var _right = _admin.querySelector(".kit-bar-right");
                // 切换右侧面板状态
                _right.classList.contains("kit-show") ? _right.classList.remove("kit-show") : _right.classList.add("kit-show");
                var handler = function (e) {
                    // 除开点击自己之外的点击都关闭
                    if (e.target.className.search("kit-bar-right") === -1) {
                        // 隐藏右侧面板
                        _right.classList.remove("kit-show");
                        // 移除当前事件
                        document.removeEventListener("click", handler);
                    }
                };
                document.addEventListener("click", handler);
            });
            return that;
        };
        /**
         *  处理顶部切换按钮功能
         */
        Basic.prototype.useEffect = function () {
            var that = this; // 获取当前类的上下文对象
            var admin = ".kit-layout-admin";
            var topbar = ".kit-topbar";
            var effect = ".kit-effect";
            var silde = ".kit-slide";
            var menu = ".kit-menu";
            var body = ".kit-body";
            var _admin = document.querySelector(admin);
            var _topbar = _admin.querySelector(topbar);
            var _effect = _topbar.querySelector(effect);
            var _silde = _admin.querySelector(silde);
            var _menu = _silde.querySelector(menu);
            var _body = _admin.querySelector(body);
            _body.style.height = window.innerHeight - 70 + "px";
            _effect.addEventListener("click", function () {
                if (_effect.className.search("kit-hide") === -1) {
                    _silde.style.marginLeft = "-200px";
                    _body.style.paddingLeft = "10px";
                    _effect.className = "kit-menu-item kit-effect kit-hide";
                }
                else {
                    _silde.style.marginLeft = "0px";
                    _body.style.paddingLeft = "210px";
                    _effect.className = "kit-menu-item kit-effect";
                }
            });
            return that;
        };
        return Basic;
    }());
    exports.default = Basic;
});
