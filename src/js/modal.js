define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * 载入动画类型
     */
    var AnimateType;
    (function (AnimateType) {
        AnimateType[AnimateType["Fade"] = 0] = "Fade";
        AnimateType[AnimateType["Bounce"] = 1] = "Bounce";
        AnimateType[AnimateType["FilpY"] = 2] = "FilpY";
        AnimateType[AnimateType["FilpX"] = 3] = "FilpX";
        AnimateType[AnimateType["Silde"] = 4] = "Silde";
        AnimateType[AnimateType["SildeLeft"] = 5] = "SildeLeft";
    })(AnimateType || (AnimateType = {}));
    /**
     * 模型框组件
     */
    var Modal = /** @class */ (function () {
        /**
         * 构造函数
         */
        function Modal() {
            /**
             *  默认参数
             */
            this.defaults = {
                title: "提示",
                content: "内容一",
                shade: true,
                onDestroyed: undefined,
                onClosed: undefined,
                onSuccessed: undefined,
                anim: AnimateType.Fade,
                shadeClose: true
            };
            // 初始化
            this.AnimateList = [
                ["fadeIn", "fadeOut"],
                ["bounceIn", "bounceOut"],
                ["flipInX", "flipOutX"],
                ["flipInY", "flipOutY"],
                ["slideInDown", "slideOutUp"],
                ["slideInLeft", "slideOutRight"],
                ["kit-anim-fadein", "kit-anim-fadeout"],
                ["kit-anim-smoothin", "kit-anim-smoothout"],
            ];
        }
        /**
         * 警告框
         * @param options 参数
         */
        Modal.prototype.alert = function (options) {
            return 1;
        };
        /**
         * modal 弹出层
         * @param options 参数
         */
        Modal.prototype.dialog = function (options) {
            var _that = this;
            // options = new Utils().merge(_that.defaults, options);
            console.log(options);
            // 获取modal的唯一id
            var times = new Date().getTime();
            // 载入动画的默认设置
            var anim = {
                in: _that.AnimateList[0][0],
                out: _that.AnimateList[0][1]
            };
            var animId = 0;
            if (options !== null && options.anim !== undefined && typeof options.anim === "number") {
                try {
                    // 如果设置的值合法则配置合法的值，否则使用默认值
                    anim = {
                        in: _that.AnimateList[options.anim][0],
                        out: _that.AnimateList[options.anim][1]
                    };
                    animId = options.anim;
                    // tslint:disable-next-line:no-empty
                }
                catch (_a) { }
            }
            // 创建modal最外层
            var _modal = document.createElement("div");
            _modal.className = "kit-modal";
            _modal.setAttribute("data-times", times.toString());
            _modal.setAttribute("data-anim", animId.toString());
            console.log((typeof options.title === "boolean" && options.title));
            // 元素隐藏
            var shade_c = options.shade === undefined || options.shade ? "" : "kit-hide";
            var title_c = options.title !== undefined &&
                (typeof options.title === "boolean" && !options.title) ? "kit-hide" : "";
            var foot_c = "";
            // 内容
            var title = options.title !== undefined ? options.title : "提示";
            // 配置modal内容
            var _arr = [
                "<div class=\"kit-modal-shade kit-anim kit-anim-fadein " + shade_c + "\"></div>",
                "<div class=\"kit-modal-dialog kit-anim " + anim.in + "\">",
                "<div class=\"kit-title  " + title_c + "\">",
                "<span>" + title + "</span>",
                "<span class=\"kit-close\">",
                "<i class=\"fa fa-close\" aria-hidden=\"true\"></i>",
                "</span>",
                "</div>",
                "<div class=\"kit-body\">",
                "This is Content...",
                "</div>",
                "<div class=\"kit-footer\">",
                "<button type=\"button\" class=\"kit-btn kit-btn-info kit-btn-sm\">确定</button>",
                "<button type=\"button\" class=\"kit-btn kit-btn-default kit-btn-sm\">取消</button>",
                "</div>"
            ];
            _modal.innerHTML = _arr.join("");
            // 追加到body末尾
            document.querySelector("body").appendChild(_modal);
            if (options !== undefined && options.onSuccessed !== undefined
                && typeof options.onSuccessed === "function") {
                // 渲染成功后触发
                options.onSuccessed(_modal, times);
            }
            // 点击遮盖层关闭
            if (options.shadeClose) {
                _modal.addEventListener("click", function (e) {
                    e.stopPropagation();
                    if (e.target.className.search("kit-modal") !== -1) {
                        _that.destroy(_modal);
                        triggerDestroyed();
                    }
                });
            }
            // 绑定右上角关闭按钮事件
            _modal.querySelector(".kit-close").addEventListener("click", function (e) {
                e.stopPropagation();
                _that.destroy(_modal);
                triggerDestroyed();
            });
            // 触发销毁后事件
            function triggerDestroyed() {
                if (options !== undefined && options.onDestroyed !== undefined
                    && typeof options.onDestroyed === "function") {
                    options.onDestroyed();
                }
            }
            return times;
        };
        /**
         * 销毁指定modal
         * @param _elem dom
         */
        Modal.prototype.destroy = function (_elem) {
            var that = this;
            // 获取设置的加载动画值
            var animId = _elem.getAttribute("data-anim");
            // 获取对应的class
            var anim = {
                in: that.AnimateList[animId][0],
                out: that.AnimateList[animId][1]
            };
            var _dialog = _elem.querySelector(".kit-modal-dialog");
            // 移除进入动画
            _dialog.classList.remove(anim.in);
            // 添加退出动画
            _dialog.classList.add(anim.out);
            var _shade = _elem.querySelector(".kit-modal-shade");
            _shade.classList.remove("kit-anim-fadein");
            _shade.classList.add("kit-anim-fadeout");
            // 设置一秒后移除dom,目的是为了让动画有一个播放的过程.
            setTimeout(function () {
                _elem.remove();
            }, 1000);
        };
        /**
         * 移除指定层
         * @param times id
         */
        Modal.prototype.close = function (times) {
            var _that = this;
            var elements = document.querySelectorAll("div[data-times=\"" + times + "\"]");
            for (var key in elements) {
                if (elements.hasOwnProperty(key)) {
                    var element = elements[key];
                    _that.destroy(element);
                }
            }
        };
        /**
         * 移除所有层
         */
        Modal.prototype.closeAll = function () {
            var _that = this;
            var elements = document.querySelectorAll("div[data-times]");
            for (var key in elements) {
                if (elements.hasOwnProperty(key)) {
                    var element = elements[key];
                    _that.destroy(element);
                }
            }
        };
        return Modal;
    }());
    exports.default = Modal;
});
