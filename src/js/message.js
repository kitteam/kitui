define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Message = /** @class */ (function () {
        function Message() {
        }
        /**
         * 打开一个加载层
         * @param options 参数
         */
        Message.prototype.spin = function (options) {
            var times = new Date().getTime();
            var text = "加载中.";
            var _elem = document.querySelector("body");
            if (options !== undefined) {
                if (options.text !== undefined && options.text.trim().length > 0) {
                    text = options.text;
                }
                if (options.elem !== undefined
                    && options.elem.trim().length > 0
                    && document.querySelector(options.elem) !== undefined) {
                    _elem = document.querySelector(options.elem);
                }
            }
            var _msg = document.createElement("div");
            _msg.className = "kit-message kit-anim kit-anim-fadein";
            _msg.setAttribute("data-times", times.toString());
            var _shade = document.createElement("div");
            _shade.classList.add("kit-shade");
            var _spin = document.createElement("div");
            _spin.classList.add("kit-spin");
            var _spin_icon = document.createElement("span");
            _spin_icon.classList.add("kit-spin-icon");
            var _spin_i = document.createElement("i");
            _spin_i.className = "fa fa-spinner";
            _spin_i.setAttribute("aria-hidden", "true");
            var _spin_text = document.createElement("p");
            _spin_text.classList.add("kit-spin-text");
            _spin_text.innerText = text;
            _spin_icon.appendChild(_spin_i);
            _spin.appendChild(_spin_icon);
            _spin.appendChild(_spin_text);
            _msg.appendChild(_shade);
            _msg.appendChild(_spin);
            _elem.appendChild(_msg);
            return times;
        };
        /**
         * 销毁指定modal
         * @param _elem dom
         */
        Message.prototype.destroy = function (_elem) {
            _elem.classList.remove("kit-anim-fadein");
            _elem.classList.add("kit-anim-fadeout");
            // 设置一秒后移除dom,目的是为了让动画有一个播放的过程.
            setTimeout(function () {
                _elem.remove();
            }, 1000);
        };
        /**
         * 移除指定层
         * @param times id
         */
        Message.prototype.close = function (times) {
            var that = this;
            var elements = document.querySelectorAll("div[data-times=\"" + times + "\"]");
            for (var key in elements) {
                if (elements.hasOwnProperty(key)) {
                    that.destroy(elements[key]);
                }
            }
        };
        /**
         * 移除所有层
         */
        Message.prototype.closeAll = function () {
            var that = this;
            var elements = document.querySelectorAll("div[data-times]");
            for (var key in elements) {
                if (elements.hasOwnProperty(key)) {
                    that.destroy(elements[key]);
                }
            }
        };
        return Message;
    }());
    exports.default = Message;
});
